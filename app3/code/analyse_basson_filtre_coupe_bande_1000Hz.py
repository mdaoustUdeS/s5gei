import matplotlib.pyplot as plt
import numpy as np
from scipy import signal
import wave
from scipy.io import wavfile
import scipy.fftpack as fftpk
import os
import IPython

#N = 6000

# Capture du son
rate, audio = wavfile.read("note_basson_plus_sinus_1000_Hz.wav")

N = len(audio)


def band_stop(n):
    w0 = 1000
    w1 = 40
    N = 6000
    k = (2 * w1 * N / 44100)
    if n == 0:
        return 1 - 2 * (k / N)
    else:
        return -2 / N * np.sin(np.pi * n * k / N) / np.sin(np.pi * n / N) * np.cos(w0 * n)


coefficent = np.arange(6000)
filtre = [band_stop(n) for n in coefficent]

#output = rate

audio = np.convolve(filtre, audio)

wavfile.write('coupe-bande.wav', rate, audio)