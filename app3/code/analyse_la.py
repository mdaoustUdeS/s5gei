import matplotlib.pyplot as plt
import numpy as np
from scipy import signal
from scipy.io import wavfile
import scipy.fftpack as fftpk
import os

N = 32
# Capture du son
file = os.path.join("", "note_guitare_LAd.wav")
#rate, audio = signal.read(file)
rate, audio = wavfile.read(file)
x1 = audio
FFT = np.fft.fft(audio)
FFT1 = np.abs(np.fft.fft(audio))
freqs = fftpk.fftfreq(len(FFT1), (1.0/(rate)))
freqs2 = []
print("audio = ", audio)

#wavfile.write('SineTest.wav', rate, audio)

#fig, axs = plt.subplots(2)
#plt.plot(freqs[range(len(FFT1)//2)], FFT1[range(len(FFT1)//2)])
plt.axis([0, 0.005, -1.5, 1.5])
#plt.stem(freqs, FFT1)


Fm = max(FFT1)
fmo = max(FFT1)*(0.01)
FFT2 = []
n = 0

#trouvons la phase
angl1 = np.angle(FFT)
angl3 = np.arctan(FFT.imag/FFT.real)
angl2 = []

peakS, _ = signal.find_peaks(FFT1)

print("peakS = ", peakS)
#plt.stem(peakS, FFT1[peakS])

freqs3 = []
for i in range(len(FFT1)//2):
    if FFT1[i] == Fm:
        for j in range(N):
            freqs3.append(freqs[i]*(j+1))
            for z in range(len(freqs)//2):
                if freqs3[j] == freqs[z]:
                    if n < N:
                        FFT2.append(FFT1[z])
                        freqs2.append(freqs3[j])
                        angl2.append(angl1[z])
                        n = n + 1
print("n =", n )
#plt.stem(freqs2, FFT2)
#plt.stem(freqs2, FFT2)
#plt.plot(peakS, FFT1[peakS])
#plt.plot(freqs2, FFT2)

print("FFT2 = ", FFT2)
print("freqs2 = ", freqs2)
plt.xlabel("Frequency (Hz)")
plt.ylabel("Amplitude ")


#fenetre utiliser np.amin

# Création du signal
#Asin(2*pi*f+phase)
x = []
LAd = 0
SOL = 0
silence = 0
FA = 0
RE = 0
MIb = 0

start_time = 0
end_time = 3
samplerate = 44100
time = np.arange(start_time, end_time, 1/samplerate)

for j in range(n):
    LAd = LAd + FFT2[j] * np.sin(2 * np.pi * (freqs2[j]) * time + angl2[j])
    SOL = SOL + FFT2[j] * np.sin(2 * np.pi * (freqs2[j] * 0.841) * time + angl2[j])
    silence = silence + FFT2[j] * np.sin(2 * np.pi * (freqs2[j] * 0) * time + angl2[j])
    FA = FA + FFT2[j] * np.sin(2 * np.pi * (freqs2[j] * 0.749) * time + angl2[j])
    RE = RE + FFT2[j] * np.sin(2 * np.pi * (freqs2[j] * 0.630) * time + angl2[j])
    MIb = MIb + FFT2[j] * np.sin(2 * np.pi * (freqs2[j] * 0.667) * time + angl2[j])

LAd = LAd/max(LAd)
SOL = SOL/max(SOL)
silence = silence/max(silence)
FA = FA/max(FA)
RE = RE/max(RE)
MIb = MIb/max(MIb)

wavfile.write('LA#.wav', samplerate, LAd)
plt.plot(time, LAd)
plt.show()
signale = np.concatenate([SOL,SOL,SOL,SOL,MIb,silence,FA,FA,FA,RE])
wavfile.write('5eSymphonieBeethoven.wav', samplerate, signale)

# 8 notes a ajouter
#SOL

