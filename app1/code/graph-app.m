%% S5 - APP 1

clc
close all

%% Cinématique - Mouvement horizontal

% Définition des paramètres du mouvement :

L0 = 0.5;                   % Longueur du bras XO (en m)
L1 = 0.25;                  % Longueur du bras OB (en m)
Omega_OB = 25;              % Vitesse angulaire du bras OB (en rad/s)
dTheta = 1e-2;              % Incrément de Theta (en rad)
Theta = [0:dTheta:pi/3];    % Plage de valeurs possibles pour Theta (en rad)

% Equations du mouvement :

Ax = 2*L1*cos(Theta);                       % Position sur x de l'outil A (en m)
V_Ax = -2*L1*Omega_OB*sin(Theta);           % Vitesse sur x de l'outil A (en m/s)
Gamma_Ax = -2*L1*(Omega_OB.^2)*cos(Theta);  % Accélération linéaire sur x de l'outil A (en m/s^2)

% Courbes :

figure(1)

subplot(3,1,1)
plot(Theta, Ax)
grid on
axis([0 pi/3 0.2 0.6])
xlabel('\theta (rad)')
ylabel('Position (m)')
title('Position Ax de A en fonction de \theta')

subplot(3,1,2)
plot(Theta, V_Ax)
grid on
axis([0 pi/3 -11 0])
xlabel('\theta (rad)')
ylabel('Vitesse (m/s)')
title('Vitesse V_Ax de A en fonction de \theta')


subplot(3,1,3)
plot(Theta, Gamma_Ax)
grid on
axis([0 pi/3 -315 -150])
xlabel('\theta (rad)')
ylabel('Accélération (m/s^2)')
title('Accélération Gamma_Ax de A en fonction de \theta')

print cin-hoz-courbe.png

% Dessin des configurations du bras de robot :

X = [0,-1*L0];  % Origine bras du robot
O = [0,0];      % Origine repère du robot

% Points A et B pour Theta = 0 :
A1 = [2*L1,0];
B1 = [L1,0];

% Points A et B pour Theta = pi/3 :
A2 = [L1,0];
B2 = [L1/2,tan(pi/3)*L1/2];

p1 = [[X,O];[O,B1];[B1,A1];];
x1 = [p1(:,1) p1(:,3)];
y1 = [p1(:,2) p1(:,4)];

p2 = [[X,O];[O,B2];[B2,A2];];
x2 = [p2(:,1) p2(:,3)];
y2 = [p2(:,2) p2(:,4)];

figure(2)

plot(x1',y1')
axis([-0.5 0.5 -0.5 0.3])
axis equal

print cin-hoz-conf-ini.png

figure(3)

plot(x2',y2')
axis([-0.5 0.5 -0.5 0.3])
axis equal

print cin-hoz-conf-fin.png

%% Cinématique - Mouvement vertical

% Définition des paramètres du mouvement :

L0 = 0.5;                   % Longueur du bras XO (en m)
L1 = 0.25;                  % Longueur du bras OB (en m)
Omega_OB = 25;              % Vitesse angulaire du bras OB (en rad/s)
dTheta = 1e-2;              % Incrément de Theta (en rad)
Theta = [0:dTheta:pi/3];    % Plage de valeurs possibles pour Theta (en rad)

% Equations du mouvement :

Ay = L1.*(sin(Theta)-sqrt(2.*cos(Theta)-(cos(Theta)).^2));                                                      % Position sur y de l'outil A (en m)
V_Ay = L1.*(Omega_OB.*cos(Theta)+sin(Theta).*Omega_OB.*(1-cos(Theta))./sqrt(2.*cos(Theta)-(cos(Theta)).^2));     % Vitesse sur y de l'outil A (en m/s)

% Courbes :

figure(4)

subplot(2,1,1)
plot(Theta, Ay)
grid on
axis([0 pi/3 -0.25 0])
xlabel('\theta (rad)')
ylabel('Position (m)')
title('Position Ay de A en fonction de \theta')

subplot(2,1,2)
plot(Theta, V_Ay)
grid on
axis([0 pi/3 5.7 6.3])
xlabel('\theta (rad)')
ylabel('Vitesse (m/s)')
title('Vitesse V_Ay de A en fonction de \theta')

print cin-ver-courbe.png

% Dessin des configurations du bras de robot :

X = [0,-1*L0];  % Origine bras du robot
O = [0,0];      % Origine repère du robot

% Points A et B pour Theta = 0 :
A3 = [L1,-1*L1];
B3 = [L1,0];

% Points A et B pour Theta = pi/3 :
A4 = [L1,0];
B4 = [L1/2,tan(pi/3)*L1/2];

p3 = [[X,O];[O,B3];[B3,A3];];
x3 = [p3(:,1) p3(:,3)];
y3 = [p3(:,2) p3(:,4)];

p4 = [[X,O];[O,B4];[B4,A4];];
x4 = [p4(:,1) p4(:,3)];
y4 = [p4(:,2) p4(:,4)];

figure(5)

plot(x3',y3')
axis([-0.5 0.5 -0.5 0.3])
axis equal

print cin-ver-conf-ini.png

figure(6)

plot(x4',y4')
axis([-0.5 0.5 -0.5 0.3])
axis equal

print cin-ver-conf-fin.png

%% Statique et Dynamique - Calcul du couple Cb

% Définition des paramètres du mouvement :

L2 = 0.25;                      % Longueur du bras BA (en m)
M_A = 0.1;                      % Masse de l'outil A (en kg)
M_BA = 1;                       % Masse du bras BA (en kg)
Alpha_BA = 5;                   % Accélération angulaire du bras BA (en rad/s)
g = 9.81;                       % Accélération gravitationnelle (en m/s^2)
dPhi = 1e-2;                    % Incrément de Phi (en rad)
Phi = [-1*pi/3: dPhi : pi/3];   % Plage de valeurs possibles pour Phi (en rad)

% Statique : Equation du couple Cb :

Cb_stat = L2*(M_A+M_BA/2)*g*cos(Phi);

% Dynamique : Equation du couple Cb :

Cb_dyna = L2*((M_A+M_BA/2)*g*cos(Phi)+Alpha_BA*(M_A+M_BA/3)*L2);

% Courbes :

figure(7)

subplot(2,1,1)
plot(Phi, Cb_stat)
grid on
axis([-1*pi/3 pi/3 0.7 1.5])
xlabel('\phi (rad)')
ylabel('Couple Cb (N.m)')
title('Couple statique Cb en fonction de \phi')

subplot(2,1,2)
plot(Phi, Cb_dyna)
grid on
axis([-1*pi/3 pi/3 0.8 1.62])
xlabel('\phi (rad)')
ylabel('Couple Cb (N.m)')
title('Couple dynamique Cb en fonction de \phi')

print sta-dyn-couple.png
