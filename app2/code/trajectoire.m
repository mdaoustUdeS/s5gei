clc
close all
clear all

dx = 1;
x = 0 : dx : 25;

xn = [0, 8, 15, 20, 25];
yn = [30, 19, 20, 16, 13];

N = numel(xn)

A = [
N, sum(xn), sum(xn.^2), sum(xn.^3), sum(xn.^4);
sum(xn), sum(xn.^2), sum(xn.^3), sum(xn.^4), sum(xn.^5);
sum(xn.^2), sum(xn.^3), sum(xn.^4), sum(xn.^5), sum(xn.^6);
sum(xn.^3), sum(xn.^4), sum(xn.^5), sum(xn.^6), sum(xn.^7);
sum(xn.^4), sum(xn.^5), sum(xn.^6), sum(xn.^7), sum(xn.^8);
];

Y = [
sum(yn);
sum(yn.*xn);
sum(yn.*xn.^2);
sum(yn.*xn.^3);
sum(yn.*xn.^4);
];

B = inv(A)*Y;

a0 = B(1)
a1 = B(2)
a2 = B(3)
a3 = B(4)
a4 = B(5)

hx = a4.*x.^4 + a3.*x.^3 + a2.*x.^2 + a1.*x + a0;

rms = sqrt(
  sum(
    (
      a4.*xn.^4 + a3.*xn.^3 + a2.*xn.^2 + a1.*xn + a0 - yn
    ).^2
  )/N
)

figure(1)
plot(xn, yn, 'ro')
hold on
plot(x, hx)
xlabel('Position (m)')
ylabel('Hauteur (m)')

uf = 0.59678
g = 9.81

print traj-y.png

speedx = sqrt(abs(2 .* -g .* (hx-30) - (2 .* uf .* g .* x)));

figure(2)
plot(x, speedx)
xlabel('Position (m)')
ylabel('Vitesse (m/s)')

print traj-v.png
