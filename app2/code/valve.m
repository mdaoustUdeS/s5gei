clc
close all
clear all

dx = 1;
x = 0 : dx : 100;

xn = [0, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100];
yn = [0.87, 0.78, 0.71, 0.61, 0.62, 0.51, 0.51, 0.49, 0.46, 0.48, 0.46];

N = numel(xn)

A = [
N, sum(xn), sum(xn.^2);
sum(xn), sum(xn.^2), sum(xn.^3);
sum(xn.^2), sum(xn.^3), sum(xn.^4);
];

Y = [
sum(yn);
sum(yn.*xn);
sum(yn.*xn.^2);
];

B = inv(A)*Y;

a0 = B(1)
a1 = B(2)
a2 = B(3)

hx = a2.*x.^2 + a1.*x + a0;

rms = sqrt(
  sum(
    (
      a2.*xn.^2 + a1.*xn + a0 - yn
    ).^2
  )/N
)

ymax = 0.59678;
xmax = interp1(hx, x, ymax)

figure(1)
plot(xn, yn, 'ro')
hold on
plot(x, hx)
plot(xmax,ymax, 'go')
xlabel('Ouverture de la valve (%)')
ylabel('Coefficient de friction dynamique')

print valve.png
